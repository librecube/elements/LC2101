# Assembly Guide

## Tools

- Screwdriver Torx T6

## Parts to buy

- 16x M2x10 ISO 14579 screws: for frame A and B assembly
- 24x M2x6 ISO 14579 screws: for frame C assembly

Optional:

- 12x M2x4 ISO 14579 screws: for side panels (top and bottom sides)
- 24x M2x6 ISO 14579 screws: for side panels (other sides)

## Parts to produce

The `build` folder contains the STL files for the parts that can be 3D printed (or manufactured in aluminium). The quantities needed are:

- 2x frame A
- 2x frame B
- 2x frame C

Optional:

- 6x side panel solid

## Instructions

### 1. Produce the frame parts

![](image_01.jpg)

### 2. Mount frames A and B

Assemble the two frames A with the two frames B using a total of sixteen M2x10 screws.

![](image_02.jpg)

### 3. Mount frames C

Mount each frame C with a total of twelve M2x6 screws.

![](image_03.jpg)

### 4. Mount solid side panels (optional)

The side panels can be mounted on each side with six M2x6 or M2x4 screws, respectively.

![](image_04.jpg)
