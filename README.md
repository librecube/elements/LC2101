# LibreCube / Platform / Structure / 1U Structure

A basic 1U CubeSat structure that can be 3D printed or produced through
CNC milling machines. The structure provides space for a stack of five LibreCube
boards or any other CubeSat compatible PC/104 style boards.

Its unique feature is that it allows easy replacement of the board stack,
without the need to disassemble the structure. Further, all sides provide
identical mounting for panels, making it extremely modular.

![](docs/image_01.png)

## How to Build

See [here](docs/assembly/README.md) for instructions.

## How to Modify

The `src` folder contains the [FreeCAD](https://www.freecadweb.org/) source files.

## Contribute

To learn more on how to successfully contribute please read the contributing
information in the [LibreCube Documentation](https://librecube.gitlab.io/).

Want to get involved? Join us at [Matrix](https://app.element.io/#/room/#librecube.org:matrix.org) or via [Email](mailto:info@librecube.org).

## License

The project is licensed under the CERN Open Hardware license.
See the [LICENSE](./LICENSE.txt) file for details.
